#!/bin/bash
source jvm-env-setup.sh
exec java "$JAVA_OPTS" -cp "$APP_CLASSPATH" "$APP_MAIN_CLASS"
