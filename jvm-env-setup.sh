#!/bin/bash

DEFAULT_JAVA_OPTS="-Djava.security.egd=file:/dev/./urandom \
-XX:+UnlockExperimentalVMOptions \
-XX:+UseCGroupMemoryLimitForHeap \
-XX:MaxRAMFraction=1"

# Join strings. First arg is the separator.
join_str() {
  local sep=$1
  shift
  local strings=("$@")
  local strings_cnt=${#strings[@]}

  local joined_str=""

  local i=0
  while ((i < strings_cnt)); do
    local str="${strings[$i]}"
    if [ -n "${str// /}" ]; then
      if ((i > 0)); then
        joined_str+="$sep$str"
      else
        joined_str+="$str"
      fi
    fi
    ((i++))
  done

  [ -n "$joined_str" ] && echo "$joined_str"
}

JAVA_OPTS=$(join_str ' ' "$DEFAULT_JAVA_OPTS" "$JAVA_OPTS")
export JAVA_OPTS
