package com.kaczoreksd.transactionfeeservice.controllers

import com.kaczoreksd.transactionfeeservice.db.documents.FeeCalculationDocument
import com.kaczoreksd.transactionfeeservice.db.repositories.FeeCalculationRepository
import com.kaczoreksd.transactionfeeservice.models.Customer
import com.kaczoreksd.transactionfeeservice.models.Transaction
import com.kaczoreksd.transactionfeeservice.models.Transactions
import com.kaczoreksd.transactionfeeservice.registries.FeeWageRegistry
import com.kaczoreksd.transactionfeeservice.services.FeeService
import com.kaczoreksd.transactionfeeservice.services.FeeServiceImpl
import spock.lang.Specification

class FeeServiceSpecification extends Specification {

    static final TRANSACTIONS_1 = [
            new Transaction("11", new BigDecimal("100"), SpecificationData.TEST_DATE),
            new Transaction("12", new BigDecimal("200"), SpecificationData.TEST_DATE),
    ].toSet()

    static final CUSTOMER_1 = new Customer("1", "Jan", "Grzib",
            new Transactions(TRANSACTIONS_1, new BigDecimal("300"), SpecificationData.TEST_DATE))

    static final TRANSACTIONS_2 = [
            new Transaction("11", new BigDecimal("1000"), SpecificationData.TEST_DATE),
            new Transaction("12", new BigDecimal("2000"), SpecificationData.TEST_DATE),
    ].toSet()

    static final CUSTOMER_2 = new Customer("2", "Jerzy", "Niewierzy",
            new Transactions(TRANSACTIONS_2, new BigDecimal("3000"), SpecificationData.TEST_DATE))


    static final FEE_WAGES = new TreeMap()
    static {
        FEE_WAGES.put(new BigDecimal("1000"), new BigDecimal("0.05"))
        FEE_WAGES.put(new BigDecimal("5000"), new BigDecimal("0.01"))
    }

    final FeeWageRegistry feeWageRegistry = Mock(FeeWageRegistry)
    final FeeCalculationRepository feeCalculationRepository = Mock(FeeCalculationRepository)

    final FeeService feeService = new FeeServiceImpl(feeWageRegistry, feeCalculationRepository)

    def setup() {
        feeWageRegistry.feeWages >> FEE_WAGES
    }

    def "should calculate fee"(Customer customer, BigDecimal expectedFee) {
        when: "fee is calculated for customer"
        def fee = feeService.calculateFee(customer)
        then: "fee has rate for given amount and event is stored in repo"
        fee == expectedFee
        1 * feeCalculationRepository.insert(_ as FeeCalculationDocument)
        where:
        customer   || expectedFee
        CUSTOMER_1 || new BigDecimal("15.00")
        CUSTOMER_2 || new BigDecimal("30.00")
    }
}