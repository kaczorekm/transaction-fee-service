package com.kaczoreksd.transactionfeeservice.controllers


import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class SpecificationData {
    static final TEST_DATE = LocalDateTime.parse("2011-12-03T10:15:30+01:00", DateTimeFormatter.ISO_DATE_TIME)
}
