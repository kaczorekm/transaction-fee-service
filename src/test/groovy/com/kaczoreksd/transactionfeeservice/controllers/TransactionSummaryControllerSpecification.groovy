package com.kaczoreksd.transactionfeeservice.controllers

import com.kaczoreksd.transactionfeeservice.models.Customer
import com.kaczoreksd.transactionfeeservice.models.TransactionSummary
import com.kaczoreksd.transactionfeeservice.services.CustomerService
import com.kaczoreksd.transactionfeeservice.services.TransactionSummaryService
import spock.lang.Specification

class TransactionSummaryControllerSpecification extends Specification {

    static final ALL_CUSTOMERS = [
            new Customer("1", "Aaron", "Ździch"),
            new Customer("2", "Heniu", "Zkrakowa")
    ]

    static final CUSTOMER_1_SUMMARY = new TransactionSummary(
            "Aaron",
            "Ździch",
            2,
            new BigDecimal(21L),
            new BigDecimal(37L),
            SpecificationData.TEST_DATE)

    static final CUSTOMER_2_SUMMARY = new TransactionSummary(
            "Heniu",
            "Zkrakowa",
            3,
            new BigDecimal(69),
            new BigDecimal(420),
            SpecificationData.TEST_DATE)

    final CustomerService customerService = Mock(CustomerService)
    final TransactionSummaryService transactionSummaryService = Mock(TransactionSummaryService)

    final TransactionSummaryController summaryController = new TransactionSummaryController(customerService,
            transactionSummaryService)

    def setup() {
        customerService.findAll() >> ALL_CUSTOMERS
        transactionSummaryService.calculateTransactionSummary(ALL_CUSTOMERS[0]) >> CUSTOMER_1_SUMMARY
        transactionSummaryService.calculateTransactionSummary(ALL_CUSTOMERS[1]) >> CUSTOMER_2_SUMMARY
    }

    def "should return all customers"() {
        when: "controller method is called without ids"
        def transactionSummaries = summaryController.getAllSummaries()
        then: "all summaries are returned"
        transactionSummaries.size() == 2
        transactionSummaries[0] == CUSTOMER_1_SUMMARY
        transactionSummaries[1] == CUSTOMER_2_SUMMARY
    }

    def "should return summary for only one customer"() {
        given: "CustomerService returns second customer"
        customerService.findByIdIn(_ as List<String>) >> [ALL_CUSTOMERS[1]]
        when: "controller method is called with ids"
        def transactionSummaries = summaryController.getSummaries(["2"])
        then: "summary for customer 2 is returned"
        transactionSummaries.size() == 1
        transactionSummaries[0] == CUSTOMER_2_SUMMARY
    }
}