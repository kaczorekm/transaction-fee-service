package com.kaczoreksd.transactionfeeservice.utils

import java.math.BigDecimal
import java.math.RoundingMode

fun BigDecimal.setMoneyScale(): BigDecimal = setScale(2, RoundingMode.HALF_UP)