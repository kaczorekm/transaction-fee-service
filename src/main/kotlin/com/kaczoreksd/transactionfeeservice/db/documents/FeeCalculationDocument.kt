package com.kaczoreksd.transactionfeeservice.db.documents

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal
import java.time.LocalDateTime

@Document
data class FeeCalculationDocument(
        @Id
        val id: ObjectId = ObjectId.get(),
        val customerId: String,
        val fee: BigDecimal,
        val date: LocalDateTime,
)