package com.kaczoreksd.transactionfeeservice.db.repositories

import com.kaczoreksd.transactionfeeservice.db.documents.FeeCalculationDocument
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository

interface FeeCalculationRepository : MongoRepository<FeeCalculationDocument, ObjectId>
