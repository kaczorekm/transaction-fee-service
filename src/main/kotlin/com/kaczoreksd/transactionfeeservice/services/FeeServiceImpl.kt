package com.kaczoreksd.transactionfeeservice.services

import com.kaczoreksd.transactionfeeservice.db.documents.FeeCalculationDocument
import com.kaczoreksd.transactionfeeservice.db.repositories.FeeCalculationRepository
import com.kaczoreksd.transactionfeeservice.models.Customer
import com.kaczoreksd.transactionfeeservice.registries.FeeWageRegistry
import com.kaczoreksd.transactionfeeservice.utils.setMoneyScale
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.LocalDateTime

@Service
class FeeServiceImpl(
        private val feeWageRegistry: FeeWageRegistry,
        private val feeCalculationRepository: FeeCalculationRepository
) : FeeService {

    override fun calculateFee(customer: Customer): BigDecimal {
        val feeWage = findFeeWage(customer.transactions.totalAmount)
        val fee = feeWage.multiply(customer.transactions.totalAmount).setMoneyScale()

        feeCalculationRepository.insert(FeeCalculationDocument(
                customerId = customer.id,
                fee = fee,
                date = LocalDateTime.now()))

        return fee
    }

    private fun findFeeWage(transactionAmount: BigDecimal): BigDecimal {
        var currentFee = BigDecimal.ZERO
        val iterator = feeWageRegistry.feeWages.iterator()
        while (iterator.hasNext()) {
            val currentFeeEntry = iterator.next()
            currentFee = currentFeeEntry.value
            if (transactionAmount < currentFeeEntry.key) {
                break
            }
        }

        return currentFee
    }
}