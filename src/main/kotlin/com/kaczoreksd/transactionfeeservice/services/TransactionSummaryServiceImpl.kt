package com.kaczoreksd.transactionfeeservice.services

import com.kaczoreksd.transactionfeeservice.models.Customer
import com.kaczoreksd.transactionfeeservice.models.TransactionSummary
import org.springframework.stereotype.Service

@Service
class TransactionSummaryServiceImpl(
        val feeService: FeeService,
) : TransactionSummaryService {

    override fun calculateTransactionSummary(customer: Customer): TransactionSummary {
        val feeAmount = feeService.calculateFee(customer)

        return TransactionSummary(
                customer.firstName,
                customer.lastName,
                customer.transactions.count(),
                customer.transactions.totalAmount,
                feeAmount,
                customer.transactions.lastDateTime,
        )
    }
}