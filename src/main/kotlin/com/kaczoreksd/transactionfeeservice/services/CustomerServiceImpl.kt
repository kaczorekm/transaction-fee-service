package com.kaczoreksd.transactionfeeservice.services

import com.kaczoreksd.transactionfeeservice.models.Customer
import com.kaczoreksd.transactionfeeservice.registries.CustomerRegistry
import org.springframework.stereotype.Service

@Service
class CustomerServiceImpl(
        val customerRegistry: CustomerRegistry,
) : CustomerService {
    override fun findById(id: String): Customer? = customerRegistry.customersById[id]
    override fun findByIdIn(ids: List<String>): List<Customer> = findAll().filter { ids.contains(it.id) }
    override fun findAll(): List<Customer> = customerRegistry.customersById.values.toList()
}