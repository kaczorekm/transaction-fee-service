package com.kaczoreksd.transactionfeeservice.services

import com.kaczoreksd.transactionfeeservice.models.Customer
import java.math.BigDecimal

interface FeeService {
    fun calculateFee(customer: Customer): BigDecimal
}