package com.kaczoreksd.transactionfeeservice.services

import com.kaczoreksd.transactionfeeservice.models.Customer
import com.kaczoreksd.transactionfeeservice.models.TransactionSummary

interface TransactionSummaryService {
    fun calculateTransactionSummary(customer: Customer): TransactionSummary
}