package com.kaczoreksd.transactionfeeservice.services

import com.kaczoreksd.transactionfeeservice.models.Customer

interface CustomerService {
    fun findById(id: String): Customer?
    fun findByIdIn(ids: List<String>): List<Customer>
    fun findAll(): List<Customer>
}