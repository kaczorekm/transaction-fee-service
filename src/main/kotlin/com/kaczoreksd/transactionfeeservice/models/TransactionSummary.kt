package com.kaczoreksd.transactionfeeservice.models

import java.math.BigDecimal
import java.time.LocalDateTime

data class TransactionSummary(
        val customerFirstName: String,
        val customerLastName: String,
        val numberOfTransactions: Int,
        val totalValueOfTransactions: BigDecimal,
        val transactionFeeValue: BigDecimal,
        val lastTransactionDateTime: LocalDateTime?
)