package com.kaczoreksd.transactionfeeservice.models

import java.math.BigDecimal
import java.time.LocalDateTime

data class Transaction(
        val id: String,
        val amount: BigDecimal,
        val dateTime: LocalDateTime,
)