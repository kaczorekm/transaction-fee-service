package com.kaczoreksd.transactionfeeservice.models

data class Customer(
        val id: String,
        val firstName: String,
        val lastName: String,
        val transactions: Transactions = Transactions(),
) {
    constructor(
            id: String,
            firstName: String,
            lastName: String,
    ) : this(id, firstName, lastName, Transactions())
}