package com.kaczoreksd.transactionfeeservice.models

import com.kaczoreksd.transactionfeeservice.utils.setMoneyScale
import java.math.BigDecimal
import java.time.LocalDateTime

data class Transactions(
        private val transactions: MutableSet<Transaction> = HashSet(),
        var totalAmount: BigDecimal = BigDecimal.ZERO.setMoneyScale(),
        var lastDateTime: LocalDateTime? = null,
) {
    fun count(): Int = transactions.size

    fun addTransaction(transaction: Transaction) {
        totalAmount = totalAmount.add(transaction.amount)
        if (lastDateTime == null || transaction.dateTime.isAfter(lastDateTime)) {
            lastDateTime = transaction.dateTime
        }

        transactions.add(transaction)
    }
}