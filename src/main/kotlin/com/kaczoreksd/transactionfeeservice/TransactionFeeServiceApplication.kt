package com.kaczoreksd.transactionfeeservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan
class TransactionFeeServiceApplication

fun main(args: Array<String>) {
    runApplication<TransactionFeeServiceApplication>(*args)
}
