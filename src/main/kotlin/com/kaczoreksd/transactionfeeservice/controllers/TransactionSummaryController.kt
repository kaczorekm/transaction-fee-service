package com.kaczoreksd.transactionfeeservice.controllers

import com.kaczoreksd.transactionfeeservice.models.TransactionSummary
import com.kaczoreksd.transactionfeeservice.services.CustomerService
import com.kaczoreksd.transactionfeeservice.services.TransactionSummaryService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/transaction-summary")
class TransactionSummaryController(
        val customerService: CustomerService,
        val transactionSummaryService: TransactionSummaryService,
) {
    @GetMapping("", "/", produces = ["application/json"])
    fun getAllSummaries(): List<TransactionSummary> = findSummariesForUsersOrAll(arrayListOf())

    @GetMapping("/{userIds}", produces = ["application/json"])
    fun getSummaries(@PathVariable userIds: List<String>): List<TransactionSummary> =
            findSummariesForUsersOrAll(userIds)

    private fun findSummariesForUsersOrAll(usersIds: List<String>) =
            (if (usersIds.isEmpty())
                customerService.findAll()
            else customerService.findByIdIn(usersIds))
                    .map(transactionSummaryService::calculateTransactionSummary)
}



