package com.kaczoreksd.transactionfeeservice.csv

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import com.kaczoreksd.transactionfeeservice.models.Customer
import com.kaczoreksd.transactionfeeservice.models.Transaction
import com.kaczoreksd.transactionfeeservice.utils.setMoneyScale
import java.io.File
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.SortedMap
import java.util.TreeMap

val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss")

fun readCustomersById(): Map<String, Customer> {
    val customers = HashMap<String, Customer>()
    val rows = csvReader().readAllWithHeader(File(findFileName("transactions.csv")))
    rows.forEach {
        var customer = rowToCustomer(it)
        if (!customers.containsKey(customer.id)) {
            customers[customer.id] = customer
        } else {
            customer = customers[customer.id]!!
        }

        customer.transactions.addTransaction(rowToTransaction(it))
    }

    return customers
}

private fun rowToCustomer(row: Map<String, String>) = Customer(
        getRequiredProp(row, "customer_id"),
        getRequiredProp(row, "customer_first_name"),
        getRequiredProp(row, "customer_last_name"))

private fun rowToTransaction(row: Map<String, String>) = Transaction(
        getRequiredProp(row, "transaction_id"),
        getRequiredProp(row, "transaction_amount").toMoney(),
        LocalDateTime.parse(getRequiredProp(row, "transaction_date"), dateTimeFormatter))

fun readTransactionFeeWagesByValue(): SortedMap<BigDecimal, BigDecimal> {
    val transactionFeeWages = TreeMap<BigDecimal, BigDecimal>()
    val rows = csvReader().readAllWithHeader(File(findFileName("fee_wages.csv")))
    rows.forEach {
        transactionFeeWages[getRequiredProp(it, "transaction_value_less_than").toMoney()] =
                getRequiredProp(it, "fee_percentage_of_transaction_value")
                        .replaceCommaWithDot()
                        .toBigDecimal()
                        .divide(BigDecimal(100L))
                        .setScale(4, RoundingMode.HALF_UP)
    }

    return transactionFeeWages
}

private fun getRequiredProp(row: Map<String, String>, propName: String): String {
    val prop = row[propName]
    if (prop.isNullOrBlank()) {
        throw IllegalArgumentException("$propName is required")
    }

    return prop
}

private fun findFileName(name: String): String =
        Thread.currentThread().contextClassLoader.getResource(name)?.file.orEmpty()

private fun String.toMoney(): BigDecimal = replaceCommaWithDot().toBigDecimal().setMoneyScale()
private fun String.replaceCommaWithDot(): String = replace(',', '.')