package com.kaczoreksd.transactionfeeservice.registries

import com.kaczoreksd.transactionfeeservice.csv.readTransactionFeeWagesByValue
import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.util.TreeMap
import javax.annotation.PostConstruct

@Component
class FeeWageRegistry {
    val feeWages = TreeMap<BigDecimal, BigDecimal>()

    @PostConstruct
    fun init() {
        feeWages.putAll(readTransactionFeeWagesByValue())
    }
}