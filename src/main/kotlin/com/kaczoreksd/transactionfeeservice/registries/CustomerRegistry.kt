package com.kaczoreksd.transactionfeeservice.registries

import com.kaczoreksd.transactionfeeservice.csv.readCustomersById
import com.kaczoreksd.transactionfeeservice.models.Customer
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
class CustomerRegistry {
    val customersById = HashMap<String, Customer>()

    @PostConstruct
    fun init() {
        customersById.putAll(readCustomersById())
    }
}