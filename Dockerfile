FROM openjdk:8-jre-slim AS create-jre-image
ARG APP_USER=1000
ARG APP_PATH=/opt/app

RUN groupadd $APP_USER && useradd $APP_USER -g $APP_USER
USER $APP_USER:$APP_USER

WORKDIR $APP_PATH
COPY --chown=$APP_USER:$APP_USER jvm-env-setup.sh bin/
COPY --chown=$APP_USER:$APP_USER entrypoint.sh bin/

ENV PATH=$PATH:${APP_PATH}/bin
ENV APP_USER=$APP_USER
ENV APP_PATH=$APP_PATH
ENV APP_CLASSPATH=.

FROM openjdk:8-jdk-slim AS unpack-fat-jar
ARG BUILD_OUTPUT=build/libs

COPY $BUILD_OUTPUT .
RUN jar -xf *.jar

FROM create-jre-image AS create-app-image

COPY --chown=$APP_USER:$APP_USER --from=unpack-fat-jar BOOT-INF/lib lib
COPY --chown=$APP_USER:$APP_USER --from=unpack-fat-jar META-INF META-INF
COPY --chown=$APP_USER:$APP_USER --from=unpack-fat-jar BOOT-INF/classes .

EXPOSE 8080

ENV APP_CLASSPATH=.:lib/*
ENV APP_MAIN_CLASS=com.kaczoreksd.transactionfeeservice.TransactionFeeServiceApplicationKt

ENTRYPOINT ["entrypoint.sh"]

