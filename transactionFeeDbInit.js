db.createUser(
    {
        user: "transaction-fee-service",
        pwd: "transaction-fee-service",
        roles: [
            {
                role: "readWrite",
                db: "transaction-fee-db"
            }
        ]
    }
)