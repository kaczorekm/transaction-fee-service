# Transaction Fee Service

## Build 
Just run gradle build

## Local deployment
1. `docker build -t transaction-fee-service .`
2. `docker-compose up` you can add `-d` to detach.

## Usage 
1. Auth method is basic
2. User passwords are in file `user.properties`
3. Transaction summary path: `http://localhost:8080/transaction-summary/{customerIds}`

## Final statements
Sorry for no Javadoc, it's a demo project and I didn't have much time this week.
Also I didn't write tests for all services, but the 2 Specifications provided should be enough.
Oh, and of course passwords are hardcoded.